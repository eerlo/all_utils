#-*- coding: utf-8 -*-
from django import template
from easy_thumbnails.files import get_thumbnailer

register = template.Library()

@register.filter(name='tabindex')
def tabindex(field, tabindex):
   return field.as_widget(attrs={"tabindex":tabindex})

@register.tag
def encaixa_imagem(parser, token):
    '''
    Syntax:
    {% encaixa_imagem instancename ratiofieldname [tamanho="WIDTHxHEIGTH"] %}
    '''
    args = token.split_contents()

    if len(args) < 3:
        # requites model and ratiofieldname
        raise template.TemplateSyntaxError("%r tag requires at least two arguments" % args[0])

    option = None
    instance = args[1]
    # strip quotes from ratio field
    ratiofieldname = args[2].strip('"\'')
    name, value = args[3].split('=')
    option = (name, value)

    return CroppingNode(instance, ratiofieldname, option)


class CroppingNode(template.Node):
    _imagem_default = u'/static/img/default-image-600x400.png'
    def __init__(self, instance, ratiofieldname, option=None):
        self.instance = instance
        self.ratiofieldname = ratiofieldname
        self.option = option

    def render(self, context):
        instance = template.Variable(self.instance).resolve(context)
        if not instance:
            return self._imagem_default

        ratiofield = instance._meta.get_field(self.ratiofieldname)
        image = getattr(instance, ratiofield.image_field)  # get imagefield
        if not image:
            return u'/static/img/default-image-600x400.png'
        if ratiofield.image_fk_field:  # image is ForeignKey
            # get the imagefield
            image = getattr(image, ratiofield.image_fk_field)

            if not image:
                return self._imagem_default

        box = getattr(instance, self.ratiofieldname)
        if ratiofield.free_crop:
            if not box:
                size = (image.width, image.height)
            else:
                box_values = list(map(int, box.split(',')))
                size = (box_values[2] - box_values[0],
                        box_values[3] - box_values[1])
        else:
            size = (int(ratiofield.width), int(ratiofield.height))

        height = float(self.option[1].split('x')[1].replace('"', ''))
        width = float(self.option[1].split('x')[0].replace('"', ''))

        #from IPython import embed;embed();
        if size[1] > size[0]:
            width = size[0] / (size[1] / height)
            height = size[1] * width / size[0]
        elif size[1] < size[0]:
            height = size[1] / (size[0] / width)
            width = height * size[0] / size[1]

        size = (int(width), int(height))

        if ratiofield.adapt_rotation:
            if (image.height > image.width) != (size[1] > size[0]):
                # box needs rotation
                size = (size[1], size[0])

        thumbnailer = get_thumbnailer(image)
        thumbnail_options = {
            'size': size,
            'box': box,
            'crop': True,
            'detail': True,
        }
        return thumbnailer.get_thumbnail(thumbnail_options).url