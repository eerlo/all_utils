# -*- coding: utf-8 -*-
"""
http://guide.python-distribute.org/index.html
http://bruno.im/2010/may/05/packaging-django-reusable-app/
http://docs.python.org/2/distutils/sourcedist.html
"""
from distutils.core import setup
from setuptools import find_packages

setup(
    name=u'all_utils',
    version=u'0.1.1',
    author=u'Eduardo Erlo',
    author_email=u'eduardo.erlo@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    url=u'',
    license=u'Eduardo Erlo License, see LICENSE.txt file',
    description=u'Aplicação com tudo em utilidades python/django.',
    long_description=open(u'README.rst').read(),
    zip_safe=False,
    install_requires=[
        u'Django==1.6.1',
        u'argparse==1.2.1',
        u'distribute==0.6.24',
        u'easy-thumbnails==2.0.1',
    ],
    test_suite='',
    tests_require=['django-discover-runner'],
)